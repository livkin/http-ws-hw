const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

let activeRoomId = null;

const setActiveRoomId = roomId => {
  activeRoomId = roomId;
};

class SocketController {

  constructor (url, handshake) {
    this.socket = io(url, handshake);
  }

  handleMessages() {
    this.socket.on('ERROR_USER_EXISTS', this.ERROR_USER_EXISTS);
    this.socket.on('USER_EXIST', this.USER_EXIST);
    this.socket.on('USER_CREATED', this.USER_CREATED);
    this.socket.on('ROOMS', this.ROOMS);
    this.socket.on('ROOM_JOINED', this.ROOM_JOINED);
    this.socket.on('ROOM_MSG', this.ROOM_MSG);
  }

  ERROR_USER_EXISTS(data) {
    alert(`ERROR_USER_EXISTS An error occured: ${data}`);
    sessionStorage.removeItem("username");
    window.location.replace("/login");
  }

  USER_EXIST(data) {
    console.log('USER_EXIST', data);
  }

  USER_CREATED(data) {
    console.log('USER_CREATED', data);
  }

  ROOMS(data) {
    console.log('ROOMS', data);
  }

  ROOM_JOINED(data) {
    setActiveRoomId(roomId);
    console.log(`I joined to ${roomId}`);
    console.log(`I in ${activeRoomId} now`);
  }

  ROOM_MSG(data) {
    console.log('ROOM_MSG', data);
  }

}

// const socket = io('http://localhost:3002', { query: { username } });

const socketController = new SocketController('http://localhost:3002', { query: { username } });

socketController.handleMessages();



socket.emit('ROOMS_SHOW');

const roomName = prompt('room name');

socket.emit('ROOM_CREATE', roomName);
socket.emit('ROOM_JOIN', roomName);

socket.emit('ROOMS_SHOW');

