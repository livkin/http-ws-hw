import * as config from "./config";
import {users, userAuth} from './users';

// const users = {};
const rooms = {
  room1:{user1: 12},
};


const getCurrentRoomId = socket => {
  Object.keys(socket.rooms).find(roomId => rooms.has(roomId))
};

export default io => {

  io.on("connection", socket => {
    
    const username = socket.handshake.query.username;
    
    userAuth(socket, username);

    socket.on('ROOMS_SHOW', () => {
      socket.emit('ROOMS', rooms);    
    })

    socket.on('ROOM_CREATE', data => {
      rooms[data] = {user2:13};
    });

    socket.on('ROOM_JOIN', roomId => {
      console.log('ROOM_JOIN', roomId);
      socket.join(roomId);
      socket.emit('ROOM_JOINED', roomId);
      io.sockets.to(roomId).emit('ROOM_MSG', `You, ${username}, are in ${roomId}`);
    })

  });
};

